#!/usr/bin/env python

import argparse
import nest

__author__ = 'rzurga'

def main(args):
    print 'Nest monitor'
    print 'User id: {}'.format(args.user)
    print 'Password: {}'.format(args.password)
    print 'Away: {}'.format(args.away)
    napi = nest.Nest(args.user, args.password)

    away = args.away

    for structure in napi.structures:
        print 'Structure %s' % structure.name
        print '    Away: %s' % structure.away
        print '    Devices:'

        for device in structure.devices:
            print '        Device: %s' % device.name
            print '            Temp: %0.1f' % device.temperature

        if not away is None and isinstance(away, bool):
            print 'Setting away to {}'.format(away)
            structure.away = away


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Nest monitor')
    parser.add_argument('-u', '--user', action='store', dest='user',
                    default='', help='User id')
    parser.add_argument('-p', '--password', action='store', dest='password',
                    default='', help='Password')
    parser.add_argument('-a', '--away', action='store', dest='away',
                    default=None, help='Away')
    args = parser.parse_args()
    main(args)
